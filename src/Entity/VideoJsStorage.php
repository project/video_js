<?php

namespace Drupal\video_js\Entity;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;


/**
 * Defines a storage class for BigvieoSource entity.
 *
 * Control file usages.
 */
class VideoJsStorage extends SqlContentEntityStorage implements VideoJsStorageInterface {

}
