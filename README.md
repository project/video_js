Use the Video JS library to apply videos to html elements on your website

## Install

Unzip library from github: https://github.com/videojs/video.js
Create /libraries/video-js folder
Copy/move video.min.js and video-js.min.css into video-js folder

This library is not published as a composer package

## Add Content

Video JS items are added as content and not configuration
Go to /admin/config/user-interface/video_js/sources to add sources
The target element is the element on the page that you wish to apply the video to.
The video is prepended to the target element



